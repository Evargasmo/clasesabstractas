/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
        private String nombre;
    private double area;

    public FiguraGeometrica(String nombre) {
        this.nombre = nombre;
    }
    
    public abstract double area();
    
    public abstract double perimetro();
    
    public abstract double volumen();
    
    
    public String toString ()
    {
       
         return "\033[34mEl área del "+nombre+" es: "+ area()
                 + "\n - \033[31mCon un perimetro de:"+perimetro ()
                 + "\n -  \033[33mY un volumen de:"+volumen ();
    }
            

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
          
}